#!/usr/bin/env python3
GENDERS = {"F": "Girls", "M": "Boys"}
CATEGORIES = (
    "Under 11",
    "Under 13",
    "Under 15",
    "Under 17",
)
RACECOUNT = 8
