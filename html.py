#!/usr/bin/env python3
import os

from constants import *

FHEAD = '<head><meta http-equiv="Content-Type" content="charset=utf-8"><link rel="icon" href="../icon.png"><title>PFR Winter Handicap</title><link rel="stylesheet" href="../style.css"></head><body><div style="width:800px;text-align:center;margin:auto"><img style="float:left;padding-right:20" src="../logo.png" alt="Penistone Footpath Runners & AC" width="128px"><h1 align="left">Penistone Footpath Runners &amp; AC</h1><h2 align="left">2019–20 Junior Winter Handicap</h2><br><br>'

NUMBERS = "0123456789"


def create_html_table(
    data,
    headingrow=0,
    columnalign=None,
    link=True,
    highlight=False,
    path="html/",
    linkables=[],
    numberlink=False,
    rotateheads=[],
    tableinfo="",
    sticky=True,
    bigbold=False,
):
    """Creates an HTML table of some data"""
    if columnalign is None:
        columnalign = ["text-align:left"]
    while len(columnalign) < len(data[0]):
        columnalign.append("text-align:center")

    # Determine how many padding (wierd) spaces are needed to fit all
    # of the numbers
    digitsneeded = []
    for i in range(0, len(data[0]), 1):
        digitsneeded.append(
            max(
                len(str(round(x[i]))) if type(x[i]) in (float, int) else 0 for x in data
            )
        )
    table = f"<table {tableinfo}>"
    for i, row in enumerate(data):
        table += "<tr>"
        for j, cell in enumerate(row):
            text = str(cell)

            if type(cell) == int:
                text = text.rjust(digitsneeded[j], " ")

            # Make it a link if needed
            if link and (os.path.exists(path + text + ".html") or text in linkables):
                text = f'<a href="{text}.html">{text}</a>'

            # Number links
            if (
                numberlink
                and any((x in text for x in NUMBERS))
                and (numberlink[0] + text).replace(numberlink[1], "") in linkables
            ):
                text = f'<a href="{numberlink[0]} {"".join([x if x in NUMBERS else "" for x in text])}.html">{text}</a>'

            # Create boldness
            if highlight:
                if row in highlight:
                    text = f"<strong>{text}</strong>"

            # Make the largest number bold
            if type(cell) == float and bigbold:
                if (
                    sorted(filter(lambda x: type(x) == float, row), reverse=True)[0]
                    == cell
                ):
                    text = f"<strong>{text}</strong>"

            # Do rotated text
            ##            if j in rotateheads and i == 0:
            ##                text = f'<div style="transform: rotate(-90deg);border-collapse:separate;height:150px;table-layout:fixed">{text}</div>'

            # Create if heading
            if i == headingrow:
                if sticky:
                    table += f'<th class="sticky" style="{columnalign[j]}">{text}</th>'
                else:
                    table += f'<th style="{columnalign[j]}">{text}</th>'

            # Empty cell optimisation
            elif text == "":
                table += "<td></td>"

            # Create standard cell
            else:
                table += f'<td style="{columnalign[j]}">{text}</td>'

        table += "</tr>"

    table += "</table>"
    return table


def create_html_page(
    pagename,
    data,
    category,
    r,
    racecount,
    headingrow=0,
    columnalign=None,
    link=True,
    highlight=False,
    path="html/",
    linkables=[],
    header="",
    numberlink=False,
    rotateheads=[],
    fellhead=False,
    bigbold=False,
):
    """Create an HTML page for some data"""
    races = ["League Table"] + [f"Race {x}" for x in range(1, racecount + 1)]
    toptable = (
        '<table style="border-collapse:separate;width:800px;table-layout:fixed"><tr>'
    )
    for gender in ("Boys", "Girls"):
        for c in CATEGORIES:
            cat = f"{c} {gender}"
            if cat == category:
                toptable += (
                    f'<th class=selected><a href="{r} – {cat}.html">{cat}</a></th>'
                )
            else:
                toptable += f'<th><a href="{r} – {cat}.html">{cat}</a></th>'
        # Gap in top row
        if gender == "Boys":
            toptable += '<th style="background-color:#000000;"></th>'
    toptable += '</tr></table><br><br><table style="border-collapse:separate;width:800px;table-layout:fixed"><tr>'
    for race in range(RACECOUNT + 1):
        try:
            if races[race] == r:
                toptable += f'<th class=selected><a href="{races[race]} – {category}.html">{races[race]}</a></th>'
            else:
                toptable += f'<th><a href="{races[race]} – {category}.html">{races[race]}</a></th>'
        except IndexError:
            toptable += f"<th>Race {race}</a></th>"
    toptable += "</table>"
    return f"{FHEAD}{toptable}<h2>{pagename}</h2>{create_html_table(data, headingrow, columnalign, link, highlight, path=path, linkables=linkables, numberlink=numberlink, rotateheads=rotateheads, bigbold=bigbold)}"
