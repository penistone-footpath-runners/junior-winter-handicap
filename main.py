#!/usr/bin/env python3
import math
import os

import html
from constants import *

PATHTOHTML = "JuniorWinterHandicap2019/"
RACEPAGEHEADINGS = ["Position", "Name", "Time", "PB?", "Race Points", "Previous PB"]
MAINHEADS = [
    "Position",
    "Name",
    "Races Run",
    "Points",
    "All-Time PB",
    "Series Best",
    "Series Average",
]
LEAGUEALIGNS = ["text-align:center", "text-align:left"]

RACEALIGNS = [
    "text-align:center",
    "text-align:left",
]


def time_mean(iterable):
    iterable = list(filter(lambda x: isinstance(x, Time), iterable))
    try:
        return sum(iterable, Time(0)) / len(iterable)
    except ZeroDivisionError:
        return Time("59:59")


class Time:
    def __init__(self, time):
        if type(time) == str:
            time = self.GetFromString(time)
        elif type(time) == Time:
            time = time.seconds
        ##        # Ensure that there are no unnecessary floats
        ##        if int(time) == time:
        ##            time = int(time)
        self.seconds = time
        self.CalculateString()

    @staticmethod
    def GetFromString(time):
        """Find a time from a string"""
        return sum(
            (0 if x == "00" else int(x)) * 60 ** i
            for i, x in enumerate(time.strip().split(":")[::-1])
        )

    def CalculateString(self):
        """Make a string out of the time"""
        if self.seconds < 6000:
            self.string = f"{self.seconds // 60}:{str(self.seconds % 60).zfill(2)}"
        else:
            hours = self.seconds // 3600
            secs = self.seconds % 3600
            self.string = (
                f"{hours}:{str(secs // 60).zfill(2)}:{str(secs % 60).zfill(2)}"
            )

    def __add__(self, time):
        return Time(self.seconds + Time(time).seconds)

    def __sub__(self, time):
        return Time(self.seconds - Time(time).seconds)

    def __truediv__(self, value):
        return Time(self.seconds / value)

    def __lt__(self, time):
        return self.seconds < Time(time).seconds

    def __le__(self, time):
        return self.seconds <= Time(time).seconds

    def __eq__(self, time):
        try:
            othertime = Time(time).seconds
        except ValueError:
            return False
        return self.seconds == othertime

    def __ne__(self, time):
        return self.seconds != Time(time).seconds

    def __ge__(self, time):
        return self.seconds >= Time(time).seconds

    def __gt__(self, time):
        return self.seconds > Time(time).seconds

    def __repr__(self):
        return f"Time('{self.string}')"

    def __str__(self):
        return self.string

    def __int__(self):
        return self.seconds

    def __round__(self):
        return Time(math.ceil(self.seconds))


class Runner:
    def __init__(self, name, gender, age_group, PB, PB_is_real):
        self.name = name
        self.gender = gender
        self.age_group = age_group

        if self.age_group == "Under 15":
            self.laps = {"M": 3, "F": 2}[self.gender]
        else:
            self.laps = {"Under 11": 1, "Under 13": 2, "Under 17": 3}[self.age_group]

        self.PB = Time(PB)
        self.RoundPB()
        if not int(PB_is_real):
            self.PB = None

        self.races = []

    def AddRace(self, race):
        """Add a new race to the runner"""
        # Make the time the actual time
        start_time = self.StartTime(race["Start Time"])
        race["Time"] = race["Time"] - start_time

        # Add the points field to the race
        if race["Position"] == 1:
            race["Race Points"] = 12
        else:
            race["Race Points"] = max(12 - race["Position"], 1)

        # Add the PB column
        race["Previous PB"] = str(self.PB)

        # Determine if the runner has a PB
        race["PB?"] = ""
        if self.PB is None:
            self.PB = race["Time"]
            self.RoundPB()

        elif race["Time"] < self.PB:
            self.PB = race["Time"]
            self.RoundPB()
            race["Race Points"] += 10
            race["PB?"] = "Yes"

        self.races.append(race)

    def GetAverage(self):
        """Find the runner's average time this year"""
        if self.races:
            return round(
                Time(
                    time_mean(
                        [
                            race["Time"]
                            for race in self.races
                            if race["Time"] != Time("59:59")
                        ]
                    )
                )
            )
        else:
            return None

    def GetBest(self):
        """Find the best time the runner has achieved this year"""
        if self.races:
            return min(race["Time"] for race in self.races)
        else:
            return None

    def GetPoints(self):
        """Calculate the number of points the runner has got in total"""
        if self.races:
            return sum(race["Race Points"] for race in self.races)
        else:
            return 0

    def RoundPB(self):
        """Round the PB to the nearest five seconds"""
        self.effective_PB = Time(math.ceil(self.PB.seconds / 5) * 5)

    def StartTime(self, start_time):
        """Calculate the race's start time."""
        if not start_time:
            start_time = worsthandicap[-1] - self.effective_PB + (self.laps - 3) * 90
        return start_time


def CSVtoTable(csv):
    """Convert a CSV string to a list of lists"""
    table = [row.split(",") for row in csv.split("\n")]
    while [""] in table:
        table.remove([""])
    table = list(filter(lambda row: not all(x == "" for x in row), table))
    return table


def CSVtoDictTable(csv, headings):
    """Convert a CSV string to a list of dicts"""
    table = CSVtoTable(csv)
    return [{headings[i]: row[i] for i in range(len(headings))} for row in table]


def DictTabletoTable(table, headings):
    return [headings] + [[row[heading] for heading in headings] for row in table]


if not os.path.exists(PATHTOHTML):
    os.makedirs(PATHTOHTML)

# Load the runner data
with open("runners.csv", "r") as f:
    runnerdata = CSVtoTable(f.read())

runners = {}

for row in runnerdata:
    # Remove 'year' column as it's useless
    row.pop(2)
    # Convert the rows to Runner objects
    runners[row[0]] = Runner(*row)

worsthandicap = [max([runner.effective_PB for runner in runners.values()])]

racefiles = [None]

for race in range(1, RACECOUNT + 1):
    try:
        with open(f"results/{race}.csv", "r") as f:
            racefiles.append(
                CSVtoDictTable(f.read(), ("Rank", "Name", "Time", "Start Time"))
            )
    except FileNotFoundError:
        break

for race in range(1, RACECOUNT + 1):
    if race >= len(racefiles):
        break
    r = f"Race {race}"
    racedata = racefiles[race]
    positions = {"F": {}, "M": {}}
    for row in racedata:
        # Make times proper times
        row["Time"] = Time(row["Time"])
        # Add age category column
        runner = runners[row["Name"]]
        row["Gender"] = runner.gender
        row["Age Group"] = runner.age_group
        # Calculate positions
        try:
            positions[row["Gender"]][row["Age Group"]] += 1
            position = positions[row["Gender"]][row["Age Group"]]
        except KeyError:
            position = 1
            positions[row["Gender"]][row["Age Group"]] = 1
        row["Position"] = position

        # Add the race to the corresponding runner
        runner.AddRace(row)
        # Use this to get average and best times
        row["Series Best"] = runner.GetBest()
        row["Average"] = runner.GetAverage()

    for gender in GENDERS:
        for category in CATEGORIES:
            cat = f"{category} {GENDERS[gender]}"
            catdata = filter(
                lambda x: x["Gender"] == gender and x["Age Group"] == category, racedata
            )
            racename = f"{r} – {cat}"
            table = DictTabletoTable(catdata, RACEPAGEHEADINGS)
            racefile = html.create_html_page(
                racename, table, cat, r, len(racefiles) - 1, columnalign=RACEALIGNS
            )
            with open(f"{PATHTOHTML}{racename}.html", "w") as f:
                f.write(racefile.replace("59:59", "None"))

    worsthandicap.append(max([x.effective_PB for x in runners.values()]))

# Create league tables
table = []
for runner in runners.values():
    table.append(
        [
            "",
            runner.name,
            len(runner.races),
            runner.GetPoints(),
            runner.PB,
            runner.GetBest(),
            runner.GetAverage(),
            runner.gender,
            runner.age_group,
        ]
    )

# Filter out those with no races
table = list(filter(lambda x: x[2] > 0, table))

for gender in GENDERS:
    for category in CATEGORIES:
        cat = f"{category} {GENDERS[gender]}"
        racename = f"League Table – {cat}"
        # Filter to only those with the same group
        filtered = list(filter(lambda x: x[7] == gender and x[8] == category, table))
        # Sort by PB, races, then points
        filtered.sort(key=lambda x: x[4])
        filtered.sort(key=lambda x: x[2], reverse=True)
        filtered.sort(key=lambda x: x[3], reverse=True)
        for runner in range(len(filtered)):
            # Add the position
            if (
                filtered[runner][3] == filtered[runner - 1][3]
                and filtered[runner] is not filtered[runner - 1]
            ):
                filtered[runner][0] = "="
            else:
                filtered[runner][0] = str(runner + 1)
            # Remove the gender and category fields, as they shouldn't
            # be in the final table; they were just needed for
            # filtering.
            filtered[runner] = filtered[runner][:-2]
        racefile = html.create_html_page(
            racename,
            [MAINHEADS] + filtered,
            cat,
            "League Table",
            len(racefiles) - 1,
            columnalign=LEAGUEALIGNS,
        )
        with open(f"{PATHTOHTML}{racename}.html", "w") as f:
            f.write(racefile.replace("59:59", "None"))

# Make the next-race handicaps
start_times = []
for runner in runners.values():
    start_times.append([runner.name, runner.laps, runner.StartTime(False)])

start_times.sort(key=lambda x: x[2])

timespage = html.create_html_page(
    "Start Times",
    [["Name", "Laps", "Start Times"]] + start_times,
    "Start Times",
    "Start Times",
    0,
)
with open(f"{PATHTOHTML}StartTimes.html", "w") as f:
    f.write(timespage.replace("59:59", "None"))
